----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:35:54 12/19/2017 
-- Design Name: 
-- Module Name:    vr_mult - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vr_mult is
    Port ( vr_a : in  STD_LOGIC_VECTOR (15 downto 0);
           vr_c : out  STD_LOGIC_VECTOR (31 downto 0));
end vr_mult;

architecture Behavioral of vr_mult is
	signal vr_prod, vr_p1, vr_p3, vr_p5 ,vr_p7, vr_p9,
vr_p10, vr_p13: unsigned(31 downto 0);
begin
	vr_p1  <= "000000000000000" & unsigned(vr_a) & "0";
	vr_p3  <= "0000000000000" & unsigned(vr_a) & "000";
	vr_p5  <= "00000000000" & unsigned(vr_a) & "00000";
	vr_p7  <= "000000000" & unsigned(vr_a) & "0000000";
	vr_p9  <= "0000000" & unsigned(vr_a) & "000000000";
	vr_p10 <= "000000" & unsigned(vr_a) & "0000000000";
	vr_p13 <= "000" & unsigned(vr_a) & "0000000000000";
	vr_prod <= vr_p13 + vr_p10 + vr_p9 + vr_p7 + vr_p5 + vr_p3 + vr_p1;
	vr_c <= std_logic_vector(vr_prod);
end Behavioral;

