--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : vr_sh.vhf
-- /___/   /\     Timestamp : 12/19/2017 23:04:12
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath D:/XI_LABS/vr_lab3/ipcore_dir -intstyle ise -family artix7 -flat -suppress -vhdl D:/XI_LABS/vr_lab3/vr_sh.vhf -w D:/XI_LABS/vr_lab3/vr_sh.sch
--Design Name: vr_sh
--Device: artix7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity vr_sh is
   port ( vr_a    : in    std_logic_vector (15 downto 0); 
          vr_c_ip : out   std_logic_vector (31 downto 0); 
          XLXN_3  : out   std_logic_vector (31 downto 0));
end vr_sh;

architecture BEHAVIORAL of vr_sh is
   component vr_mult
      port ( vr_a : in    std_logic_vector (15 downto 0); 
             vr_c : out   std_logic_vector (31 downto 0));
   end component;
   
   component vr_ip
      port ( a : in    std_logic_vector (15 downto 0); 
             p : out   std_logic_vector (31 downto 0));
   end component;
   
begin
   XLXI_1 : vr_mult
      port map (vr_a(15 downto 0)=>vr_a(15 downto 0),
                vr_c(31 downto 0)=>XLXN_3(31 downto 0));
   
   XLXI_2 : vr_ip
      port map (a(15 downto 0)=>vr_a(15 downto 0),
                p(31 downto 0)=>vr_c_ip(31 downto 0));
   
end BEHAVIORAL;


