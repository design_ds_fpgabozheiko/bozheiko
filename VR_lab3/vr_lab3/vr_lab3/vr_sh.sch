<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="vr_a(15:0)" />
        <signal name="XLXN_2(15:0)" />
        <signal name="XLXN_3(31:0)" />
        <signal name="vr_c_ip(31:0)" />
        <port polarity="Input" name="vr_a(15:0)" />
        <port polarity="Output" name="XLXN_3(31:0)" />
        <port polarity="Output" name="vr_c_ip(31:0)" />
        <blockdef name="vr_mult">
            <timestamp>2017-12-19T21:0:19</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="vr_ip">
            <timestamp>2017-12-19T21:2:44</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="vr_mult" name="XLXI_1">
            <blockpin signalname="vr_a(15:0)" name="vr_a(15:0)" />
            <blockpin signalname="XLXN_3(31:0)" name="vr_c(31:0)" />
        </block>
        <block symbolname="vr_ip" name="XLXI_2">
            <blockpin signalname="vr_a(15:0)" name="a(15:0)" />
            <blockpin signalname="vr_c_ip(31:0)" name="p(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="464" y="240" name="XLXI_1" orien="R0">
        </instance>
        <instance x="496" y="272" name="XLXI_2" orien="R0">
        </instance>
        <branch name="vr_a(15:0)">
            <wire x2="384" y1="208" y2="208" x1="240" />
            <wire x2="448" y1="208" y2="208" x1="384" />
            <wire x2="464" y1="208" y2="208" x1="448" />
            <wire x2="384" y1="208" y2="352" x1="384" />
            <wire x2="496" y1="352" y2="352" x1="384" />
        </branch>
        <branch name="XLXN_3(31:0)">
            <wire x2="1248" y1="208" y2="208" x1="848" />
        </branch>
        <branch name="vr_c_ip(31:0)">
            <wire x2="1088" y1="352" y2="352" x1="1072" />
            <wire x2="1104" y1="352" y2="352" x1="1088" />
            <wire x2="1120" y1="352" y2="352" x1="1104" />
            <wire x2="1248" y1="352" y2="352" x1="1120" />
        </branch>
        <iomarker fontsize="28" x="240" y="208" name="vr_a(15:0)" orien="R180" />
        <iomarker fontsize="28" x="1248" y="208" name="XLXN_3(31:0)" orien="R0" />
        <iomarker fontsize="28" x="1248" y="352" name="vr_c_ip(31:0)" orien="R0" />
    </sheet>
</drawing>