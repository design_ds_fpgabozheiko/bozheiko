--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:51:31 12/19/2017
-- Design Name:   
-- Module Name:   D:/XI_LABS/vr_lab3/test.vhd
-- Project Name:  vr_lab3
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: vr_mult
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test IS
END test;
 
ARCHITECTURE behavior OF test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT vr_mult
    PORT(
         vr_a : IN  std_logic_vector(15 downto 0);
         vr_c : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    
	signal vr_a : STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0001";
   --Inputs
   signal vr_c : std_logic_vector (31 downto 0);
 
   
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: vr_mult PORT MAP (
          vr_a => vr_a,
          vr_c => vr_c
        );

   -- Clock process definitions
   
 

   -- Stimulus process
   stim_proc: process
   begin		
      vr_a <= x"1234" after 10 ns;
      wait for 10 ns;
   end process;

END;
