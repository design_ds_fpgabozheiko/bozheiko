----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:14:48 12/02/2017 
-- Design Name: 
-- Module Name:    kp_mult - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kp_mult is
    Port ( kp_a : in  STD_LOGIC_VECTOR (15 downto 0);
           kp_b : in  STD_LOGIC_VECTOR (15 downto 0);
           kp_c : out  STD_LOGIC_VECTOR (31 downto 0));
end kp_mult;

architecture Behavioral of kp_mult is
	constant width: integer:=16;
	signal kp_a0,kp_a1,kp_a2,kp_a3,kp_a4,kp_a5,kp_a6,kp_a7,kp_a8,kp_a9,
kp_a10,kp_a11,kp_a12,kp_a13,kp_a14,kp_a15:std_logic_vector(width-1 downto 0);
	signal kp_b0,kp_b1,kp_b2,kp_b3,kp_b4,kp_b5,kp_b6,kp_b7,kp_b8,kp_b9,
kp_b10,kp_b11,kp_b12,kp_b13,kp_b14,kp_b15:std_logic_vector(width-1 downto 0);
	signal kp_bv0,kp_bv1,kp_bv2,kp_bv3,kp_bv4,kp_bv5,kp_bv6,kp_bv7,kp_bv8,kp_bv9,
kp_bv10,kp_bv11,kp_bv12,kp_bv13,kp_bv14,kp_bv15:std_logic_vector(width-1 downto 0);
	signal kp_bp0,kp_bp1,kp_bp2,kp_bp3,kp_bp4,kp_bp5,kp_bp6,kp_bp7,kp_bp8,kp_bp9,
kp_bp10,kp_bp11,kp_bp12,kp_bp13,kp_bp14,kp_bp15:unsigned(2*width-1 downto 0);
	signal kp_pp0,kp_pp1,kp_pp2,kp_pp3,kp_pp4,kp_pp5,kp_pp6,kp_pp7,kp_pp8,kp_pp9,
kp_pp10,kp_pp11,kp_pp12,kp_pp13,kp_pp14,kp_pp15:unsigned(2*width-1 downto 0);

begin
	--stage 0
	kp_bv0 <= (others => kp_b(0));
	kp_bp0 <= unsigned("0000000000000000" & (kp_bv0 and kp_a));
	kp_pp0 <= kp_bp0;
	kp_a0 <= kp_a;
	kp_b0 <= kp_b;
	--stage 1
	kp_bv1 <= (others => kp_b0(1));
	kp_bp1 <= unsigned("000000000000000" & (kp_bv1 and kp_a0) & "0");
	kp_pp1 <= kp_pp0 + kp_bp1;
	kp_a1 <= kp_a0;
	kp_b1 <= kp_b0;
	--stage 2
	kp_bv2 <= (others => kp_b1(2));
	kp_bp2 <= unsigned("00000000000000" & (kp_bv2 and kp_a1) & "00");
	kp_pp2 <= kp_pp1 + kp_bp2;
	kp_a2 <= kp_a1;
	kp_b2 <= kp_b1;
	--stage 3
	kp_bv3 <= (others => kp_b2(3));
	kp_bp3 <= unsigned("0000000000000" & (kp_bv3 and kp_a2) & "000");
	kp_pp3 <= kp_pp2 + kp_bp3;
	kp_a3 <= kp_a2;
	kp_b3 <= kp_b2;
	--stage 4
	kp_bv4 <= (others => kp_b3(4));
	kp_bp4 <= unsigned("000000000000" & (kp_bv4 and kp_a3) & "0000");
	kp_pp4 <= kp_pp3 + kp_bp4;
	kp_a4 <= kp_a3;
	kp_b4 <= kp_b3;
	--stage 5
	kp_bv5 <= (others => kp_b4(5));
	kp_bp5 <= unsigned("00000000000" & (kp_bv5 and kp_a4) & "00000");
	kp_pp5 <= kp_pp4 + kp_bp5;
	kp_a5 <= kp_a4;
	kp_b5 <= kp_b4;
	--stage 6
	kp_bv6 <= (others => kp_b5(6));
	kp_bp6 <= unsigned("0000000000" & (kp_bv6 and kp_a5) & "000000");
	kp_pp6 <= kp_pp5 + kp_bp6;
	kp_a6 <= kp_a5;
	kp_b6 <= kp_b5;
	--stage 7
	kp_bv7 <= (others => kp_b6(7));
	kp_bp7 <= unsigned("000000000" & (kp_bv7 and kp_a6) & "0000000");
	kp_pp7 <= kp_pp6 + kp_bp7;
	kp_a7 <= kp_a6;
	kp_b7 <= kp_b6;
	--stage 8
	kp_bv8 <= (others => kp_b7(8));
	kp_bp8 <= unsigned("00000000" & (kp_bv8 and kp_a7) & "00000000");
	kp_pp8 <= kp_pp7 + kp_bp8;
	kp_a8 <= kp_a7;
	kp_b8 <= kp_b7;
	--stage 9
	kp_bv9 <= (others => kp_b8(9));
	kp_bp9 <= unsigned("0000000" & (kp_bv9 and kp_a8) & "000000000");
	kp_pp9 <= kp_pp8 + kp_bp9;
	kp_a9 <= kp_a8;
	kp_b9 <= kp_b8;
	--stage 10
	kp_bv10 <= (others => kp_b9(10));
	kp_bp10 <= unsigned("000000" & (kp_bv10 and kp_a9) & "0000000000");
	kp_pp10 <= kp_pp9 + kp_bp10;
	kp_a10 <= kp_a9;
	kp_b10 <= kp_b9;
	--stage 11
	kp_bv11 <= (others => kp_b10(11));
	kp_bp11 <= unsigned("00000" & (kp_bv11 and kp_a10) & "00000000000");
	kp_pp11 <= kp_pp10 + kp_bp11;
	kp_a11 <= kp_a10;
	kp_b11 <= kp_b10;
	--stage 12
	kp_bv12 <= (others => kp_b11(12));
	kp_bp12 <= unsigned("0000" & (kp_bv12 and kp_a11) & "000000000000");
	kp_pp12 <= kp_pp11 + kp_bp12;
	kp_a12 <= kp_a11;
	kp_b12 <= kp_b11;
	--stage 13
	kp_bv13 <= (others => kp_b12(13));
	kp_bp13 <= unsigned("000" & (kp_bv13 and kp_a12) & "0000000000000");
	kp_pp13 <= kp_pp12 + kp_bp13;
	kp_a13 <= kp_a12;
	kp_b13 <= kp_b12;
	--stage 14
	kp_bv14 <= (others => kp_b13(14));
	kp_bp14 <= unsigned("00" & (kp_bv14 and kp_a13) & "00000000000000");
	kp_pp14 <= kp_pp13 + kp_bp14;
	kp_a14 <= kp_a13;
	kp_b14 <= kp_b13;
	--stage 15
	kp_bv15 <= (others => kp_b14(15));
	kp_bp15 <= unsigned("0" & (kp_bv15 and kp_a14) & "000000000000000");
	kp_pp15 <= kp_pp14 + kp_bp15;
	--result
	kp_c <= std_logic_vector(kp_pp15);

end Behavioral;

