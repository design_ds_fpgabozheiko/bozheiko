-- Vhdl test bench created from schematic D:\XI_LABS\KP_lab4\KP_test_mult.sch - Sat Dec 02 17:19:15 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY KP_test_mult_KP_test_mult_sch_tb IS
END KP_test_mult_KP_test_mult_sch_tb;
ARCHITECTURE behavioral OF KP_test_mult_KP_test_mult_sch_tb IS 

   COMPONENT KP_test_mult
   PORT( KP_a	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0) ; 
          KP_b	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0) ; 
          KP_c	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0));
   END COMPONENT;

   SIGNAL KP_a	:	STD_LOGIC_VECTOR (15 DOWNTO 0):= x"1234";
   SIGNAL KP_b	:	STD_LOGIC_VECTOR (15 DOWNTO 0):= x"3456";
   SIGNAL KP_c	:	STD_LOGIC_VECTOR (31 DOWNTO 0);

BEGIN

   UUT: KP_test_mult PORT MAP(
		KP_a => KP_a, 
		KP_b => KP_b, 
		KP_c => KP_c
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		KP_a <= x"4321" after 10 ns;
		KP_b <= x"7891" after 10 ns;
      WAIT for 10 ns; -- will wait forever
		KP_a <= x"FFFF" after 10 ns;
		KP_b <= x"E45A" after 10 ns;
      WAIT for 10 ns; 
		
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
