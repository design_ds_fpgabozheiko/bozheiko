--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : KP_test_sh.vhf
-- /___/   /\     Timestamp : 12/02/2017 17:50:18
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family artix7 -flat -suppress -vhdl D:/XI_LABS/KP_lab4/KP_test_sh.vhf -w D:/XI_LABS/KP_lab4/KP_test_sh.sch
--Design Name: KP_test_sh
--Device: artix7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity KP_test_sh is
   port ( KP_a : in    std_logic_vector (15 downto 0); 
          KP_b : in    std_logic_vector (15 downto 0); 
          KP_c : out   std_logic_vector (31 downto 0));
end KP_test_sh;

architecture BEHAVIORAL of KP_test_sh is
   component KP_mult
      port ( KP_a : in    std_logic_vector (15 downto 0); 
             KP_b : in    std_logic_vector (15 downto 0); 
             KP_c : out   std_logic_vector (31 downto 0));
   end component;
   
begin
   XLXI_1 : KP_mult
      port map (KP_a(15 downto 0)=>KP_a(15 downto 0),
                KP_b(15 downto 0)=>KP_b(15 downto 0),
                KP_c(31 downto 0)=>KP_c(31 downto 0));
   
end BEHAVIORAL;


