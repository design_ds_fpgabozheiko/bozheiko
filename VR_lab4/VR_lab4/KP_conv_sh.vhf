--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : KP_conv_sh.vhf
-- /___/   /\     Timestamp : 12/18/2017 09:19:36
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family artix7 -flat -suppress -vhdl D:/XI_LABS/KP_lab4/KP_conv_sh.vhf -w D:/XI_LABS/KP_lab4/KP_conv_sh.sch
--Design Name: KP_conv_sh
--Device: artix7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--
----- CELL FD16CE_HXILINX_KP_conv_sh -----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FD16CE_HXILINX_KP_conv_sh is
port (
    Q   : out STD_LOGIC_VECTOR(15 downto 0) := (others => '0');

    C   : in STD_LOGIC;
    CE  : in STD_LOGIC;
    CLR : in STD_LOGIC;
    D   : in STD_LOGIC_VECTOR(15 downto 0)
    );
end FD16CE_HXILINX_KP_conv_sh;

architecture Behavioral of FD16CE_HXILINX_KP_conv_sh is

begin

process(C, CLR)
begin
  if (CLR='1') then
    Q <= (others => '0');
  elsif (C'event and C = '1') then
    if (CE='1') then 
      Q <= D;
    end if;
  end if;
end process;


end Behavioral;

----- CELL ADD16_HXILINX_KP_conv_sh -----
  
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ADD16_HXILINX_KP_conv_sh is
port(
       CO  : out std_logic;
       OFL : out std_logic;
       S   : out std_logic_vector(15 downto 0);
    
       A   : in std_logic_vector(15 downto 0);
       B   : in std_logic_vector(15 downto 0);
       CI  : in std_logic
    );
end ADD16_HXILINX_KP_conv_sh;

architecture ADD16_HXILINX_KP_conv_sh_V of ADD16_HXILINX_KP_conv_sh is
  signal adder_tmp: std_logic_vector(16 downto 0);
begin
  adder_tmp <= conv_std_logic_vector((conv_integer(A) + conv_integer(B) + conv_integer(CI)),17);
  S         <= adder_tmp(15 downto 0);
  CO        <= adder_tmp(16);
  OFL <=  ( A(15) and B(15) and (not adder_tmp(15)) ) or ( (not A(15)) and (not B(15)) and adder_tmp(15) );  
          
end ADD16_HXILINX_KP_conv_sh_V;
 
----- CELL ADSU16_HXILINX_KP_conv_sh -----
  
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ADSU16_HXILINX_KP_conv_sh is
port(
    CO   : out std_logic;
    OFL  : out std_logic;
    S    : out std_logic_vector(15 downto 0);

    A    : in std_logic_vector(15 downto 0);
    ADD  : in std_logic;
    B    : in std_logic_vector(15 downto 0);
    CI   : in std_logic
  );
end ADSU16_HXILINX_KP_conv_sh;

architecture ADSU16_HXILINX_KP_conv_sh_V of ADSU16_HXILINX_KP_conv_sh is

begin
  adsu_p : process (A, ADD, B, CI)
    variable adsu_tmp : std_logic_vector(16 downto 0);
  begin
    if(ADD = '1') then
     adsu_tmp := conv_std_logic_vector((conv_integer(A) + conv_integer(B) + conv_integer(CI)),17);
    else
     adsu_tmp := conv_std_logic_vector((conv_integer(A) - conv_integer(not CI) - conv_integer(B)),17);
  end if;
      
  S   <= adsu_tmp(15 downto 0);
   
  if (ADD='1') then
    CO <= adsu_tmp(16);
    OFL <=  ( A(15) and B(15) and (not adsu_tmp(15)) ) or ( (not A(15)) and (not B(15)) and adsu_tmp(15) );  
  else
    CO <= not adsu_tmp(16);
    OFL <=  ( A(15) and (not B(15)) and (not adsu_tmp(15)) ) or ( (not A(15)) and B(15) and adsu_tmp(15) );  
  end if;
 
  end process;
  
end ADSU16_HXILINX_KP_conv_sh_V;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity KP_conv_sh is
   port ( KP_A         : in    std_logic_vector (15 downto 0); 
          KP_B         : in    std_logic_vector (15 downto 0); 
          KP_ce        : in    std_logic; 
          KP_clk       : in    std_logic; 
          KP_D         : in    std_logic_vector (15 downto 0); 
          KP_reset     : in    std_logic; 
          KP_T         : in    std_logic_vector (15 downto 0); 
          KP_CO_fin    : out   std_logic; 
          KP_CO_sum    : out   std_logic; 
          KP_OFL_fin   : out   std_logic; 
          KP_OFL_sum   : out   std_logic; 
          KP_res_great : out   std_logic_vector (15 downto 0); 
          KP_res_less  : out   std_logic_vector (15 downto 0));
end KP_conv_sh;

architecture BEHAVIORAL of KP_conv_sh is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal ab                    : std_logic_vector (31 downto 0);
   signal ab_great_reg          : std_logic_vector (31 downto 16);
   signal ab_less_reg           : std_logic_vector (15 downto 0);
   signal d2                    : std_logic_vector (31 downto 0);
   signal d2_great_reg          : std_logic_vector (31 downto 16);
   signal d2_less_reg           : std_logic_vector (15 downto 0);
   signal sum_great             : std_logic_vector (31 downto 16);
   signal sum_g_reg             : std_logic_vector (31 downto 16);
   signal sum_less              : std_logic_vector (15 downto 0);
   signal sum_l_reg             : std_logic_vector (15 downto 0);
   signal t_reg_1               : std_logic_vector (15 downto 0);
   signal t_reg_2               : std_logic_vector (15 downto 0);
   signal XLXN_16               : std_logic;
   signal XLXN_49               : std_logic;
   signal XLXN_118              : std_logic;
   signal XLXN_123              : std_logic;
   signal XLXN_124              : std_logic_vector (15 downto 0);
   signal XLXN_129              : std_logic_vector (15 downto 0);
   signal XLXN_130              : std_logic_vector (15 downto 0);
   signal XLXI_30_CI_openSignal : std_logic;
   component KP_tree_pipe_mult
      port ( KP_res : in    std_logic; 
             KP_clk : in    std_logic; 
             KP_A   : in    std_logic_vector (15 downto 0); 
             KP_B   : in    std_logic_vector (15 downto 0); 
             KP_C   : out   std_logic_vector (31 downto 0));
   end component;
   
   component FD16CE_HXILINX_KP_conv_sh
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic_vector (15 downto 0); 
             Q   : out   std_logic_vector (15 downto 0));
   end component;
   
   component ADD16_HXILINX_KP_conv_sh
      port ( A   : in    std_logic_vector (15 downto 0); 
             B   : in    std_logic_vector (15 downto 0); 
             CI  : in    std_logic; 
             CO  : out   std_logic; 
             OFL : out   std_logic; 
             S   : out   std_logic_vector (15 downto 0));
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component ADSU16_HXILINX_KP_conv_sh
      port ( A   : in    std_logic_vector (15 downto 0); 
             ADD : in    std_logic; 
             B   : in    std_logic_vector (15 downto 0); 
             CI  : in    std_logic; 
             CO  : out   std_logic; 
             OFL : out   std_logic; 
             S   : out   std_logic_vector (15 downto 0));
   end component;
   
   attribute HU_SET of XLXI_3 : label is "XLXI_3_0";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_5";
   attribute HU_SET of XLXI_5 : label is "XLXI_5_6";
   attribute HU_SET of XLXI_6 : label is "XLXI_6_1";
   attribute HU_SET of XLXI_7 : label is "XLXI_7_2";
   attribute HU_SET of XLXI_8 : label is "XLXI_8_3";
   attribute HU_SET of XLXI_9 : label is "XLXI_9_4";
   attribute HU_SET of XLXI_27 : label is "XLXI_27_7";
   attribute HU_SET of XLXI_28 : label is "XLXI_28_8";
   attribute HU_SET of XLXI_29 : label is "XLXI_29_9";
   attribute HU_SET of XLXI_30 : label is "XLXI_30_10";
   attribute HU_SET of XLXI_31 : label is "XLXI_31_11";
   attribute HU_SET of XLXI_35 : label is "XLXI_35_12";
   attribute HU_SET of XLXI_36 : label is "XLXI_36_13";
begin
   XLXN_124(15 downto 0) <= x"0000";
   XLXI_1 : KP_tree_pipe_mult
      port map (KP_A(15 downto 0)=>KP_A(15 downto 0),
                KP_B(15 downto 0)=>KP_B(15 downto 0),
                KP_clk=>KP_clk,
                KP_res=>KP_reset,
                KP_C(31 downto 0)=>ab(31 downto 0));
   
   XLXI_2 : KP_tree_pipe_mult
      port map (KP_A(15 downto 0)=>KP_D(15 downto 0),
                KP_B(15 downto 0)=>KP_D(15 downto 0),
                KP_clk=>KP_clk,
                KP_res=>KP_reset,
                KP_C(31 downto 0)=>d2(31 downto 0));
   
   XLXI_3 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>KP_T(15 downto 0),
                Q(15 downto 0)=>t_reg_1(15 downto 0));
   
   XLXI_4 : ADD16_HXILINX_KP_conv_sh
      port map (A(15 downto 0)=>ab_less_reg(15 downto 0),
                B(15 downto 0)=>d2_less_reg(15 downto 0),
                CI=>XLXN_49,
                CO=>XLXN_16,
                OFL=>open,
                S(15 downto 0)=>sum_less(15 downto 0));
   
   XLXI_5 : ADD16_HXILINX_KP_conv_sh
      port map (A(15 downto 0)=>ab_great_reg(31 downto 16),
                B(15 downto 0)=>d2_great_reg(31 downto 16),
                CI=>XLXN_16,
                CO=>KP_CO_sum,
                OFL=>KP_OFL_sum,
                S(15 downto 0)=>sum_great(31 downto 16));
   
   XLXI_6 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>ab(15 downto 0),
                Q(15 downto 0)=>ab_less_reg(15 downto 0));
   
   XLXI_7 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>ab(31 downto 16),
                Q(15 downto 0)=>ab_great_reg(31 downto 16));
   
   XLXI_8 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>d2(15 downto 0),
                Q(15 downto 0)=>d2_less_reg(15 downto 0));
   
   XLXI_9 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>d2(31 downto 16),
                Q(15 downto 0)=>d2_great_reg(31 downto 16));
   
   XLXI_16 : GND
      port map (G=>XLXN_49);
   
   XLXI_27 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>sum_less(15 downto 0),
                Q(15 downto 0)=>sum_l_reg(15 downto 0));
   
   XLXI_28 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>sum_great(31 downto 16),
                Q(15 downto 0)=>sum_g_reg(31 downto 16));
   
   XLXI_29 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>t_reg_1(15 downto 0),
                Q(15 downto 0)=>t_reg_2(15 downto 0));
   
   XLXI_30 : ADSU16_HXILINX_KP_conv_sh
      port map (A(15 downto 0)=>sum_l_reg(15 downto 0),
                ADD=>XLXN_123,
                B(15 downto 0)=>t_reg_2(15 downto 0),
                CI=>XLXI_30_CI_openSignal,
                CO=>XLXN_118,
                OFL=>open,
                S(15 downto 0)=>XLXN_129(15 downto 0));
   
   XLXI_31 : ADSU16_HXILINX_KP_conv_sh
      port map (A(15 downto 0)=>sum_g_reg(31 downto 16),
                ADD=>XLXN_123,
                B(15 downto 0)=>XLXN_124(15 downto 0),
                CI=>XLXN_118,
                CO=>KP_CO_fin,
                OFL=>KP_OFL_fin,
                S(15 downto 0)=>XLXN_130(15 downto 0));
   
   XLXI_32 : GND
      port map (G=>XLXN_123);
   
   XLXI_35 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>XLXN_129(15 downto 0),
                Q(15 downto 0)=>KP_res_less(15 downto 0));
   
   XLXI_36 : FD16CE_HXILINX_KP_conv_sh
      port map (C=>KP_clk,
                CE=>KP_ce,
                CLR=>KP_reset,
                D(15 downto 0)=>XLXN_130(15 downto 0),
                Q(15 downto 0)=>KP_res_great(15 downto 0));
   
end BEHAVIORAL;


