<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="KP_A(15:0)" />
        <signal name="KP_B(15:0)" />
        <signal name="KP_D(15:0)" />
        <signal name="KP_T(15:0)" />
        <signal name="KP_reset" />
        <signal name="KP_clk" />
        <signal name="KP_ce" />
        <signal name="ab(31:0)" />
        <signal name="d2(31:0)" />
        <signal name="ab_less_reg(15:0)" />
        <signal name="ab_great_reg(31:16)" />
        <signal name="d2_less_reg(15:0)" />
        <signal name="d2_great_reg(31:16)" />
        <signal name="ab(15:0)" />
        <signal name="ab(31:16)" />
        <signal name="d2(15:0)" />
        <signal name="d2(31:16)" />
        <signal name="XLXN_16" />
        <signal name="KP_CO_sum" />
        <signal name="KP_OFL_sum" />
        <signal name="sum_less(15:0)" />
        <signal name="sum_great(31:16)" />
        <signal name="XLXN_49" />
        <signal name="t_reg_1(15:0)" />
        <signal name="sum_l_reg(15:0)" />
        <signal name="sum_g_reg(31:16)" />
        <signal name="t_reg_2(15:0)" />
        <signal name="XLXN_118" />
        <signal name="KP_CO_fin" />
        <signal name="KP_OFL_fin" />
        <signal name="XLXN_123" />
        <signal name="XLXN_124(15:0)" />
        <signal name="XLXN_129(15:0)" />
        <signal name="XLXN_130(15:0)" />
        <signal name="KP_res_less(15:0)" />
        <signal name="KP_res_great(15:0)" />
        <port polarity="Input" name="KP_A(15:0)" />
        <port polarity="Input" name="KP_B(15:0)" />
        <port polarity="Input" name="KP_D(15:0)" />
        <port polarity="Input" name="KP_T(15:0)" />
        <port polarity="Input" name="KP_reset" />
        <port polarity="Input" name="KP_clk" />
        <port polarity="Input" name="KP_ce" />
        <port polarity="Output" name="KP_CO_sum" />
        <port polarity="Output" name="KP_OFL_sum" />
        <port polarity="Output" name="KP_CO_fin" />
        <port polarity="Output" name="KP_OFL_fin" />
        <port polarity="Output" name="KP_res_less(15:0)" />
        <port polarity="Output" name="KP_res_great(15:0)" />
        <blockdef name="KP_tree_pipe_mult">
            <timestamp>2017-12-12T9:29:17</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="adsu16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="constant">
            <timestamp>2006-1-1T10:10:10</timestamp>
            <rect width="112" x="0" y="0" height="64" />
            <line x2="112" y1="32" y2="32" x1="144" />
        </blockdef>
        <block symbolname="KP_tree_pipe_mult" name="XLXI_1">
            <blockpin signalname="KP_reset" name="KP_res" />
            <blockpin signalname="KP_clk" name="KP_clk" />
            <blockpin signalname="KP_A(15:0)" name="KP_A(15:0)" />
            <blockpin signalname="KP_B(15:0)" name="KP_B(15:0)" />
            <blockpin signalname="ab(31:0)" name="KP_C(31:0)" />
        </block>
        <block symbolname="KP_tree_pipe_mult" name="XLXI_2">
            <blockpin signalname="KP_reset" name="KP_res" />
            <blockpin signalname="KP_clk" name="KP_clk" />
            <blockpin signalname="KP_D(15:0)" name="KP_A(15:0)" />
            <blockpin signalname="KP_D(15:0)" name="KP_B(15:0)" />
            <blockpin signalname="d2(31:0)" name="KP_C(31:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_3">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="KP_T(15:0)" name="D(15:0)" />
            <blockpin signalname="t_reg_1(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_6">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="ab(15:0)" name="D(15:0)" />
            <blockpin signalname="ab_less_reg(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_7">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="ab(31:16)" name="D(15:0)" />
            <blockpin signalname="ab_great_reg(31:16)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_8">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="d2(15:0)" name="D(15:0)" />
            <blockpin signalname="d2_less_reg(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_9">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="d2(31:16)" name="D(15:0)" />
            <blockpin signalname="d2_great_reg(31:16)" name="Q(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_4">
            <blockpin signalname="ab_less_reg(15:0)" name="A(15:0)" />
            <blockpin signalname="d2_less_reg(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_49" name="CI" />
            <blockpin signalname="XLXN_16" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="sum_less(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_5">
            <blockpin signalname="ab_great_reg(31:16)" name="A(15:0)" />
            <blockpin signalname="d2_great_reg(31:16)" name="B(15:0)" />
            <blockpin signalname="XLXN_16" name="CI" />
            <blockpin signalname="KP_CO_sum" name="CO" />
            <blockpin signalname="KP_OFL_sum" name="OFL" />
            <blockpin signalname="sum_great(31:16)" name="S(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_16">
            <blockpin signalname="XLXN_49" name="G" />
        </block>
        <block symbolname="fd16ce" name="XLXI_27">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="sum_less(15:0)" name="D(15:0)" />
            <blockpin signalname="sum_l_reg(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_28">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="sum_great(31:16)" name="D(15:0)" />
            <blockpin signalname="sum_g_reg(31:16)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_29">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="t_reg_1(15:0)" name="D(15:0)" />
            <blockpin signalname="t_reg_2(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="adsu16" name="XLXI_30">
            <blockpin signalname="sum_l_reg(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_123" name="ADD" />
            <blockpin signalname="t_reg_2(15:0)" name="B(15:0)" />
            <blockpin name="CI" />
            <blockpin signalname="XLXN_118" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_129(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="adsu16" name="XLXI_31">
            <blockpin signalname="sum_g_reg(31:16)" name="A(15:0)" />
            <blockpin signalname="XLXN_123" name="ADD" />
            <blockpin signalname="XLXN_124(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_118" name="CI" />
            <blockpin signalname="KP_CO_fin" name="CO" />
            <blockpin signalname="KP_OFL_fin" name="OFL" />
            <blockpin signalname="XLXN_130(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_32">
            <blockpin signalname="XLXN_123" name="G" />
        </block>
        <block symbolname="constant" name="XLXI_34">
            <attr value="0" name="CValue">
                <trait delete="all:1 sym:0" />
                <trait editname="all:1 sch:0" />
                <trait valuetype="BitVector 32 Hexadecimal" />
            </attr>
            <blockpin signalname="XLXN_124(15:0)" name="O" />
        </block>
        <block symbolname="fd16ce" name="XLXI_35">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="XLXN_129(15:0)" name="D(15:0)" />
            <blockpin signalname="KP_res_less(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_36">
            <blockpin signalname="KP_clk" name="C" />
            <blockpin signalname="KP_ce" name="CE" />
            <blockpin signalname="KP_reset" name="CLR" />
            <blockpin signalname="XLXN_130(15:0)" name="D(15:0)" />
            <blockpin signalname="KP_res_great(15:0)" name="Q(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="1056" y="576" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1056" y="976" name="XLXI_2" orien="R0">
        </instance>
        <branch name="KP_A(15:0)">
            <wire x2="1056" y1="480" y2="480" x1="704" />
        </branch>
        <branch name="KP_B(15:0)">
            <wire x2="1056" y1="544" y2="544" x1="704" />
        </branch>
        <branch name="KP_D(15:0)">
            <wire x2="944" y1="880" y2="880" x1="704" />
            <wire x2="1056" y1="880" y2="880" x1="944" />
            <wire x2="944" y1="880" y2="944" x1="944" />
            <wire x2="1056" y1="944" y2="944" x1="944" />
        </branch>
        <branch name="KP_T(15:0)">
            <wire x2="2016" y1="1776" y2="1776" x1="688" />
            <wire x2="2032" y1="1776" y2="1776" x1="2016" />
        </branch>
        <iomarker fontsize="28" x="704" y="480" name="KP_A(15:0)" orien="R180" />
        <iomarker fontsize="28" x="704" y="544" name="KP_B(15:0)" orien="R180" />
        <iomarker fontsize="28" x="704" y="880" name="KP_D(15:0)" orien="R180" />
        <branch name="KP_reset">
            <wire x2="960" y1="352" y2="352" x1="704" />
            <wire x2="1056" y1="352" y2="352" x1="960" />
            <wire x2="960" y1="352" y2="752" x1="960" />
            <wire x2="1056" y1="752" y2="752" x1="960" />
            <wire x2="960" y1="752" y2="2000" x1="960" />
            <wire x2="2032" y1="2000" y2="2000" x1="960" />
        </branch>
        <branch name="KP_clk">
            <wire x2="992" y1="416" y2="416" x1="704" />
            <wire x2="1056" y1="416" y2="416" x1="992" />
            <wire x2="992" y1="416" y2="816" x1="992" />
            <wire x2="1056" y1="816" y2="816" x1="992" />
            <wire x2="992" y1="816" y2="1904" x1="992" />
            <wire x2="2032" y1="1904" y2="1904" x1="992" />
        </branch>
        <branch name="KP_ce">
            <wire x2="1456" y1="624" y2="624" x1="704" />
            <wire x2="1456" y1="624" y2="1840" x1="1456" />
            <wire x2="2032" y1="1840" y2="1840" x1="1456" />
        </branch>
        <iomarker fontsize="28" x="704" y="624" name="KP_ce" orien="R180" />
        <iomarker fontsize="28" x="704" y="416" name="KP_clk" orien="R180" />
        <iomarker fontsize="28" x="704" y="352" name="KP_reset" orien="R180" />
        <branch name="ab(31:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1488" y="352" type="branch" />
            <wire x2="1488" y1="352" y2="352" x1="1440" />
            <wire x2="1552" y1="352" y2="352" x1="1488" />
        </branch>
        <branch name="d2(31:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1488" y="752" type="branch" />
            <wire x2="1488" y1="752" y2="752" x1="1440" />
            <wire x2="1552" y1="752" y2="752" x1="1488" />
        </branch>
        <instance x="2032" y="608" name="XLXI_6" orien="R0" />
        <instance x="2032" y="960" name="XLXI_7" orien="R0" />
        <instance x="2032" y="1312" name="XLXI_8" orien="R0" />
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="480" type="branch" />
            <wire x2="1968" y1="480" y2="480" x1="1952" />
            <wire x2="2032" y1="480" y2="480" x1="1968" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="576" type="branch" />
            <wire x2="1984" y1="576" y2="576" x1="1952" />
            <wire x2="2032" y1="576" y2="576" x1="1984" />
        </branch>
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="832" type="branch" />
            <wire x2="1984" y1="832" y2="832" x1="1952" />
            <wire x2="2032" y1="832" y2="832" x1="1984" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="928" type="branch" />
            <wire x2="1984" y1="928" y2="928" x1="1952" />
            <wire x2="2032" y1="928" y2="928" x1="1984" />
        </branch>
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="1184" type="branch" />
            <wire x2="1968" y1="1184" y2="1184" x1="1952" />
            <wire x2="2032" y1="1184" y2="1184" x1="1968" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="1280" type="branch" />
            <wire x2="1984" y1="1280" y2="1280" x1="1952" />
            <wire x2="2032" y1="1280" y2="1280" x1="1984" />
        </branch>
        <instance x="2032" y="1664" name="XLXI_9" orien="R0" />
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="1536" type="branch" />
            <wire x2="1984" y1="1536" y2="1536" x1="1952" />
            <wire x2="2032" y1="1536" y2="1536" x1="1984" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="1632" type="branch" />
            <wire x2="1968" y1="1632" y2="1632" x1="1952" />
            <wire x2="2032" y1="1632" y2="1632" x1="1968" />
        </branch>
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="1472" type="branch" />
            <wire x2="1984" y1="1472" y2="1472" x1="1952" />
            <wire x2="2032" y1="1472" y2="1472" x1="1984" />
        </branch>
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="1120" type="branch" />
            <wire x2="1968" y1="1120" y2="1120" x1="1952" />
            <wire x2="2032" y1="1120" y2="1120" x1="1968" />
        </branch>
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="768" type="branch" />
            <wire x2="1984" y1="768" y2="768" x1="1952" />
            <wire x2="2032" y1="768" y2="768" x1="1984" />
        </branch>
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="416" type="branch" />
            <wire x2="1984" y1="416" y2="416" x1="1952" />
            <wire x2="2032" y1="416" y2="416" x1="1984" />
        </branch>
        <branch name="ab_less_reg(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="352" type="branch" />
            <wire x2="2464" y1="352" y2="352" x1="2416" />
            <wire x2="2512" y1="352" y2="352" x1="2464" />
        </branch>
        <branch name="ab_great_reg(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="704" type="branch" />
            <wire x2="2464" y1="704" y2="704" x1="2416" />
            <wire x2="2512" y1="704" y2="704" x1="2464" />
        </branch>
        <branch name="d2_less_reg(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="1056" type="branch" />
            <wire x2="2464" y1="1056" y2="1056" x1="2416" />
            <wire x2="2512" y1="1056" y2="1056" x1="2464" />
        </branch>
        <branch name="d2_great_reg(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2448" y="1408" type="branch" />
            <wire x2="2448" y1="1408" y2="1408" x1="2416" />
            <wire x2="2528" y1="1408" y2="1408" x1="2448" />
        </branch>
        <branch name="ab(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1936" y="352" type="branch" />
            <wire x2="1936" y1="352" y2="352" x1="1856" />
            <wire x2="2032" y1="352" y2="352" x1="1936" />
        </branch>
        <branch name="ab(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1936" y="704" type="branch" />
            <wire x2="1936" y1="704" y2="704" x1="1856" />
            <wire x2="2000" y1="704" y2="704" x1="1936" />
            <wire x2="2032" y1="704" y2="704" x1="2000" />
        </branch>
        <branch name="d2(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1952" y="1056" type="branch" />
            <wire x2="1952" y1="1056" y2="1056" x1="1872" />
            <wire x2="2016" y1="1056" y2="1056" x1="1952" />
            <wire x2="2032" y1="1056" y2="1056" x1="2016" />
        </branch>
        <branch name="d2(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1952" y="1408" type="branch" />
            <wire x2="1952" y1="1408" y2="1408" x1="1872" />
            <wire x2="2032" y1="1408" y2="1408" x1="1952" />
        </branch>
        <instance x="2944" y="672" name="XLXI_4" orien="R0" />
        <instance x="2944" y="1248" name="XLXI_5" orien="R0" />
        <branch name="XLXN_16">
            <wire x2="2944" y1="704" y2="800" x1="2944" />
            <wire x2="3392" y1="704" y2="704" x1="2944" />
            <wire x2="3392" y1="608" y2="704" x1="3392" />
        </branch>
        <branch name="KP_CO_sum">
            <wire x2="3408" y1="1184" y2="1184" x1="3392" />
            <wire x2="3440" y1="1184" y2="1184" x1="3408" />
        </branch>
        <branch name="KP_OFL_sum">
            <wire x2="3408" y1="1120" y2="1120" x1="3392" />
            <wire x2="3440" y1="1120" y2="1120" x1="3408" />
        </branch>
        <branch name="ab_less_reg(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2784" y="352" type="branch" />
            <wire x2="2784" y1="352" y2="352" x1="2752" />
            <wire x2="2944" y1="352" y2="352" x1="2784" />
        </branch>
        <branch name="d2_less_reg(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2800" y="480" type="branch" />
            <wire x2="2800" y1="480" y2="480" x1="2752" />
            <wire x2="2944" y1="480" y2="480" x1="2800" />
        </branch>
        <branch name="ab_great_reg(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2800" y="928" type="branch" />
            <wire x2="2800" y1="928" y2="928" x1="2768" />
            <wire x2="2944" y1="928" y2="928" x1="2800" />
        </branch>
        <branch name="d2_great_reg(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2816" y="1056" type="branch" />
            <wire x2="2816" y1="1056" y2="1056" x1="2768" />
            <wire x2="2944" y1="1056" y2="1056" x1="2816" />
        </branch>
        <branch name="sum_less(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3520" y="416" type="branch" />
            <wire x2="3520" y1="416" y2="416" x1="3392" />
            <wire x2="3616" y1="416" y2="416" x1="3520" />
        </branch>
        <branch name="sum_great(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3504" y="992" type="branch" />
            <wire x2="3504" y1="992" y2="992" x1="3392" />
            <wire x2="3616" y1="992" y2="992" x1="3504" />
        </branch>
        <instance x="2640" y="160" name="XLXI_16" orien="R90" />
        <branch name="XLXN_49">
            <wire x2="2944" y1="224" y2="224" x1="2768" />
        </branch>
        <iomarker fontsize="28" x="3440" y="1120" name="KP_OFL_sum" orien="R0" />
        <iomarker fontsize="28" x="3440" y="1184" name="KP_CO_sum" orien="R0" />
        <instance x="2032" y="2032" name="XLXI_3" orien="R0" />
        <branch name="t_reg_1(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2480" y="1776" type="branch" />
            <wire x2="2480" y1="1776" y2="1776" x1="2416" />
            <wire x2="2560" y1="1776" y2="1776" x1="2480" />
        </branch>
        <iomarker fontsize="28" x="688" y="1776" name="KP_T(15:0)" orien="R180" />
        <instance x="4000" y="672" name="XLXI_27" orien="R0" />
        <instance x="4000" y="1056" name="XLXI_28" orien="R0" />
        <instance x="4000" y="1472" name="XLXI_29" orien="R0" />
        <branch name="t_reg_1(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3904" y="1216" type="branch" />
            <wire x2="3904" y1="1216" y2="1216" x1="3872" />
            <wire x2="4000" y1="1216" y2="1216" x1="3904" />
        </branch>
        <branch name="sum_great(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3904" y="800" type="branch" />
            <wire x2="3904" y1="800" y2="800" x1="3872" />
            <wire x2="4000" y1="800" y2="800" x1="3904" />
        </branch>
        <branch name="sum_less(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3920" y="416" type="branch" />
            <wire x2="3920" y1="416" y2="416" x1="3872" />
            <wire x2="4000" y1="416" y2="416" x1="3920" />
        </branch>
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3952" y="480" type="branch" />
            <wire x2="3952" y1="480" y2="480" x1="3888" />
            <wire x2="4000" y1="480" y2="480" x1="3952" />
        </branch>
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3936" y="544" type="branch" />
            <wire x2="3936" y1="544" y2="544" x1="3888" />
            <wire x2="4000" y1="544" y2="544" x1="3936" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3920" y="640" type="branch" />
            <wire x2="3920" y1="640" y2="640" x1="3888" />
            <wire x2="4000" y1="640" y2="640" x1="3920" />
        </branch>
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3936" y="864" type="branch" />
            <wire x2="3936" y1="864" y2="864" x1="3888" />
            <wire x2="4000" y1="864" y2="864" x1="3936" />
        </branch>
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3920" y="928" type="branch" />
            <wire x2="3920" y1="928" y2="928" x1="3888" />
            <wire x2="4000" y1="928" y2="928" x1="3920" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3920" y="1024" type="branch" />
            <wire x2="3920" y1="1024" y2="1024" x1="3888" />
            <wire x2="4000" y1="1024" y2="1024" x1="3920" />
        </branch>
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3936" y="1280" type="branch" />
            <wire x2="3936" y1="1280" y2="1280" x1="3888" />
            <wire x2="4000" y1="1280" y2="1280" x1="3936" />
        </branch>
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3920" y="1344" type="branch" />
            <wire x2="3920" y1="1344" y2="1344" x1="3888" />
            <wire x2="4000" y1="1344" y2="1344" x1="3920" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3920" y="1440" type="branch" />
            <wire x2="3920" y1="1440" y2="1440" x1="3888" />
            <wire x2="4000" y1="1440" y2="1440" x1="3920" />
        </branch>
        <branch name="sum_l_reg(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="4480" y="416" type="branch" />
            <wire x2="4400" y1="416" y2="416" x1="4384" />
            <wire x2="4480" y1="416" y2="416" x1="4400" />
            <wire x2="4528" y1="416" y2="416" x1="4480" />
        </branch>
        <branch name="sum_g_reg(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="4480" y="800" type="branch" />
            <wire x2="4480" y1="800" y2="800" x1="4384" />
            <wire x2="4528" y1="800" y2="800" x1="4480" />
        </branch>
        <branch name="t_reg_2(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="4448" y="1216" type="branch" />
            <wire x2="4448" y1="1216" y2="1216" x1="4384" />
            <wire x2="4528" y1="1216" y2="1216" x1="4448" />
        </branch>
        <instance x="4976" y="720" name="XLXI_30" orien="R0" />
        <instance x="4976" y="1184" name="XLXI_31" orien="R0" />
        <branch name="XLXN_118">
            <wire x2="4976" y1="688" y2="736" x1="4976" />
            <wire x2="5424" y1="688" y2="688" x1="4976" />
            <wire x2="5424" y1="656" y2="688" x1="5424" />
        </branch>
        <instance x="4848" y="1296" name="XLXI_32" orien="R0" />
        <branch name="KP_CO_fin">
            <wire x2="5488" y1="1120" y2="1120" x1="5424" />
        </branch>
        <branch name="KP_OFL_fin">
            <wire x2="5504" y1="1056" y2="1056" x1="5424" />
        </branch>
        <iomarker fontsize="28" x="5504" y="1056" name="KP_OFL_fin" orien="R0" />
        <iomarker fontsize="28" x="5488" y="1120" name="KP_CO_fin" orien="R0" />
        <branch name="XLXN_123">
            <wire x2="4976" y1="656" y2="656" x1="4912" />
            <wire x2="4912" y1="656" y2="1120" x1="4912" />
            <wire x2="4976" y1="1120" y2="1120" x1="4912" />
            <wire x2="4912" y1="1120" y2="1168" x1="4912" />
        </branch>
        <branch name="XLXN_124(15:0)">
            <wire x2="4976" y1="992" y2="992" x1="4960" />
            <wire x2="4960" y1="992" y2="1184" x1="4960" />
            <wire x2="5024" y1="1184" y2="1184" x1="4960" />
            <wire x2="5024" y1="1184" y2="1424" x1="5024" />
            <wire x2="5024" y1="1424" y2="1424" x1="4960" />
        </branch>
        <instance x="4816" y="1392" name="XLXI_34" orien="R0">
        </instance>
        <branch name="sum_l_reg(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="4880" y="400" type="branch" />
            <wire x2="4880" y1="400" y2="400" x1="4816" />
            <wire x2="4976" y1="400" y2="400" x1="4880" />
        </branch>
        <branch name="t_reg_2(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="4848" y="528" type="branch" />
            <wire x2="4848" y1="528" y2="528" x1="4816" />
            <wire x2="4976" y1="528" y2="528" x1="4848" />
        </branch>
        <branch name="sum_g_reg(31:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="4880" y="864" type="branch" />
            <wire x2="4880" y1="864" y2="864" x1="4816" />
            <wire x2="4976" y1="864" y2="864" x1="4880" />
        </branch>
        <branch name="XLXN_129(15:0)">
            <wire x2="5952" y1="464" y2="464" x1="5424" />
        </branch>
        <branch name="XLXN_130(15:0)">
            <wire x2="5952" y1="928" y2="928" x1="5424" />
        </branch>
        <instance x="5952" y="720" name="XLXI_35" orien="R0" />
        <instance x="5952" y="1184" name="XLXI_36" orien="R0" />
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5904" y="528" type="branch" />
            <wire x2="5904" y1="528" y2="528" x1="5840" />
            <wire x2="5952" y1="528" y2="528" x1="5904" />
        </branch>
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5888" y="592" type="branch" />
            <wire x2="5856" y1="592" y2="592" x1="5840" />
            <wire x2="5888" y1="592" y2="592" x1="5856" />
            <wire x2="5952" y1="592" y2="592" x1="5888" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5904" y="688" type="branch" />
            <wire x2="5904" y1="688" y2="688" x1="5840" />
            <wire x2="5952" y1="688" y2="688" x1="5904" />
        </branch>
        <branch name="KP_ce">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5904" y="992" type="branch" />
            <wire x2="5904" y1="992" y2="992" x1="5856" />
            <wire x2="5952" y1="992" y2="992" x1="5904" />
        </branch>
        <branch name="KP_clk">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5904" y="1056" type="branch" />
            <wire x2="5904" y1="1056" y2="1056" x1="5856" />
            <wire x2="5952" y1="1056" y2="1056" x1="5904" />
        </branch>
        <branch name="KP_reset">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5920" y="1152" type="branch" />
            <wire x2="5920" y1="1152" y2="1152" x1="5856" />
            <wire x2="5952" y1="1152" y2="1152" x1="5920" />
        </branch>
        <branch name="KP_res_less(15:0)">
            <wire x2="6560" y1="464" y2="464" x1="6336" />
        </branch>
        <branch name="KP_res_great(15:0)">
            <wire x2="6560" y1="928" y2="928" x1="6336" />
        </branch>
        <iomarker fontsize="28" x="6560" y="464" name="KP_res_less(15:0)" orien="R0" />
        <iomarker fontsize="28" x="6560" y="928" name="KP_res_great(15:0)" orien="R0" />
    </sheet>
</drawing>