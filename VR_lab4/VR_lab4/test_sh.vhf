--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : test_sh.vhf
-- /___/   /\     Timestamp : 12/11/2017 16:02:07
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family artix7 -flat -suppress -vhdl D:/XI_LABS/KP_lab4/test_sh.vhf -w D:/XI_LABS/KP_lab4/test_sh.sch
--Design Name: test_sh
--Device: artix7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity test_sh is
   port ( KP_A : in    std_logic_vector (15 downto 0); 
          KP_B : in    std_logic_vector (15 downto 0); 
          KP_C : out   std_logic_vector (31 downto 0));
end test_sh;

architecture BEHAVIORAL of test_sh is
   component kp_mult
      port ( kp_a : in    std_logic_vector (15 downto 0); 
             kp_b : in    std_logic_vector (15 downto 0); 
             kp_c : out   std_logic_vector (31 downto 0));
   end component;
   
begin
   XLXI_1 : kp_mult
      port map (kp_a(15 downto 0)=>KP_A(15 downto 0),
                kp_b(15 downto 0)=>KP_B(15 downto 0),
                kp_c(31 downto 0)=>KP_C(31 downto 0));
   
end BEHAVIORAL;


