<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="KP_a(15:0)" />
        <signal name="KP_b(15:0)" />
        <signal name="KP_c(31:0)" />
        <port polarity="Input" name="KP_a(15:0)" />
        <port polarity="Input" name="KP_b(15:0)" />
        <port polarity="Output" name="KP_c(31:0)" />
        <blockdef name="KP_mult">
            <timestamp>2017-12-2T15:17:49</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="KP_mult" name="XLXI_1">
            <blockpin signalname="KP_a(15:0)" name="KP_a(15:0)" />
            <blockpin signalname="KP_b(15:0)" name="KP_b(15:0)" />
            <blockpin signalname="KP_c(31:0)" name="KP_c(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="608" y="368" name="XLXI_1" orien="R0">
        </instance>
        <branch name="KP_a(15:0)">
            <wire x2="608" y1="272" y2="272" x1="448" />
        </branch>
        <branch name="KP_b(15:0)">
            <wire x2="608" y1="336" y2="336" x1="448" />
        </branch>
        <branch name="KP_c(31:0)">
            <wire x2="1136" y1="272" y2="272" x1="992" />
        </branch>
        <iomarker fontsize="28" x="448" y="272" name="KP_a(15:0)" orien="R180" />
        <iomarker fontsize="28" x="448" y="336" name="KP_b(15:0)" orien="R180" />
        <iomarker fontsize="28" x="1136" y="272" name="KP_c(31:0)" orien="R0" />
    </sheet>
</drawing>