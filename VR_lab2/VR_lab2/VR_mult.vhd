----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:20:42 11/30/2017 
-- Design Name: 
-- Module Name:    VR_mult - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VR_mult is
    Port ( VR_a : in  STD_LOGIC_VECTOR (7 downto 0);
           VR_b : in  STD_LOGIC_VECTOR (7 downto 0);
           VR_c : out  STD_LOGIC_VECTOR (15 downto 0));
end VR_mult;

architecture Behavioral of VR_mult is
	constant width: INTEGER:=8;
	signal vr_ua, vr_b0, vr_b1, vr_b2, vr_b3, vr_b4,vr_b5, vr_b6, vr_b7 : unsigned (width-1 downto 0);
	signal vr_p, vr_p0, vr_p1, vr_p2, vr_p3, vr_p4, vr_p5, vr_p6, vr_p7 : unsigned (2*width-1 downto 0);
begin
	vr_ua <= unsigned(vr_A);
	vr_b0 <= (others => vr_B(0));
	vr_b1 <= (others => vr_B(1));
	vr_b2 <= (others => vr_B(2));
	vr_b3 <= (others => vr_B(3));
	vr_b4 <= (others => vr_B(4));
	vr_b5 <= (others => vr_B(5));
	vr_b6 <= (others => vr_B(6));
	vr_b7 <= (others => vr_B(7));
	vr_p0 <= "00000000" & (vr_b0 and vr_ua);
	vr_p1 <= "0000000" & (vr_b1 and vr_ua) & "0";
	vr_p2 <= "000000" & (vr_b2 and vr_ua) & "00";
	vr_p3 <= "00000" & (vr_b3 and vr_ua) & "000";
	vr_p4 <= "0000" & (vr_b4 and vr_ua) & "0000";
	vr_p5 <= "000" & (vr_b5 and vr_ua) & "00000";
	vr_p6 <= "00" & (vr_b6 and vr_ua) & "000000";
	vr_p7 <= "0" & (vr_b7 and vr_ua) & "0000000";
	
	vr_p <= ((vr_p0+vr_p1)+(vr_p2+vr_p3))+((vr_p4+vr_p5)+(vr_p6+vr_p7));
	
	VR_C<= std_logic_vector (vr_p);

end Behavioral;

