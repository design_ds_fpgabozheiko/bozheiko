<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_7(7:0)" />
        <signal name="XLXN_8(7:0)" />
        <signal name="XLXN_11(7:0)" />
        <signal name="XLXN_12(7:0)" />
        <signal name="VR_c_m(15:0)" />
        <signal name="VR_c_mm(15:0)" />
        <signal name="VR_c_ip(15:0)" />
        <signal name="VR_a(7:0)" />
        <signal name="VR_b(7:0)" />
        <port polarity="Output" name="VR_c_m(15:0)" />
        <port polarity="Output" name="VR_c_mm(15:0)" />
        <port polarity="Output" name="VR_c_ip(15:0)" />
        <port polarity="Input" name="VR_a(7:0)" />
        <port polarity="Input" name="VR_b(7:0)" />
        <blockdef name="VR_mm">
            <timestamp>2017-11-30T10:47:22</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="VR_mult">
            <timestamp>2017-11-30T10:47:36</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="VR_ip_m">
            <timestamp>2017-11-30T10:45:13</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="VR_mult" name="XLXI_2">
            <blockpin signalname="VR_a(7:0)" name="VR_a(7:0)" />
            <blockpin signalname="VR_b(7:0)" name="VR_b(7:0)" />
            <blockpin signalname="VR_c_m(15:0)" name="VR_c(15:0)" />
        </block>
        <block symbolname="VR_mm" name="XLXI_1">
            <blockpin signalname="VR_a(7:0)" name="VR_a(7:0)" />
            <blockpin signalname="VR_b(7:0)" name="VR_b(7:0)" />
            <blockpin signalname="VR_c_mm(15:0)" name="VR_c(15:0)" />
        </block>
        <block symbolname="VR_ip_m" name="XLXI_3">
            <blockpin signalname="VR_a(7:0)" name="a(7:0)" />
            <blockpin signalname="VR_b(7:0)" name="b(7:0)" />
            <blockpin signalname="VR_c_ip(15:0)" name="p(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="720" y="272" name="XLXI_2" orien="R0">
        </instance>
        <instance x="720" y="480" name="XLXI_1" orien="R0">
        </instance>
        <instance x="752" y="528" name="XLXI_3" orien="R0">
        </instance>
        <branch name="VR_c_m(15:0)">
            <wire x2="1712" y1="176" y2="176" x1="1104" />
        </branch>
        <branch name="VR_c_mm(15:0)">
            <wire x2="1712" y1="384" y2="384" x1="1104" />
        </branch>
        <branch name="VR_c_ip(15:0)">
            <wire x2="1712" y1="608" y2="608" x1="1328" />
        </branch>
        <branch name="VR_a(7:0)">
            <wire x2="608" y1="176" y2="176" x1="384" />
            <wire x2="656" y1="176" y2="176" x1="608" />
            <wire x2="720" y1="176" y2="176" x1="656" />
            <wire x2="656" y1="176" y2="384" x1="656" />
            <wire x2="720" y1="384" y2="384" x1="656" />
            <wire x2="608" y1="176" y2="608" x1="608" />
            <wire x2="752" y1="608" y2="608" x1="608" />
        </branch>
        <branch name="VR_b(7:0)">
            <wire x2="512" y1="240" y2="240" x1="384" />
            <wire x2="560" y1="240" y2="240" x1="512" />
            <wire x2="576" y1="240" y2="240" x1="560" />
            <wire x2="720" y1="240" y2="240" x1="576" />
            <wire x2="560" y1="240" y2="448" x1="560" />
            <wire x2="720" y1="448" y2="448" x1="560" />
            <wire x2="512" y1="240" y2="672" x1="512" />
            <wire x2="752" y1="672" y2="672" x1="512" />
        </branch>
        <iomarker fontsize="28" x="384" y="176" name="VR_a(7:0)" orien="R180" />
        <iomarker fontsize="28" x="384" y="240" name="VR_b(7:0)" orien="R180" />
        <iomarker fontsize="28" x="1712" y="176" name="VR_c_m(15:0)" orien="R0" />
        <iomarker fontsize="28" x="1712" y="384" name="VR_c_mm(15:0)" orien="R0" />
        <iomarker fontsize="28" x="1712" y="608" name="VR_c_ip(15:0)" orien="R0" />
    </sheet>
</drawing>