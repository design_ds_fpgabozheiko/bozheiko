----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:27:50 11/30/2017 
-- Design Name: 
-- Module Name:    VR_mm - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VR_mm is
    Port ( VR_a : in  STD_LOGIC_VECTOR (7 downto 0);
           VR_b : in  STD_LOGIC_VECTOR (7 downto 0);
           VR_c : out  STD_LOGIC_VECTOR (15 downto 0));
end VR_mm;

architecture Behavioral of VR_mm is
	constant width: integer:=8;
	
	signal vr_ua, vr_bv0, vr_bv1, vr_bv2, vr_bv3, vr_bv4, vr_bv5, vr_bv6, vr_bv7 : 
	unsigned (width-1 downto 0);
	signal vr_p0, vr_p1, vr_p2, vr_p3, vr_p4, vr_p5, vr_p6, vr_p7 : 
	unsigned (width downto 0);
	signal vr_product : unsigned(2*width-1 downto 0);
begin
	vr_ua <= unsigned(vr_A);
	vr_bv0 <= (others => VR_b(0));
	vr_bv1 <= (others => VR_b(1));
	vr_bv2 <= (others => VR_b(2));
	vr_bv3 <= (others => VR_b(3));
	vr_bv4 <= (others => VR_b(4));
	vr_bv5 <= (others => VR_b(5));
	vr_bv6 <= (others => VR_b(6));
	vr_bv7 <= (others => VR_b(7));
	vr_p0 <= "0" & (vr_bv0 and vr_ua);
	vr_p1 <= ("0" & vr_p0(width downto 1)) + ("0" & (vr_bv1 and vr_ua));
	vr_p2 <= ("0" & vr_p1(width downto 1)) + ("0" & (vr_bv2 and vr_ua));
	vr_p3 <= ("0" & vr_p2(width downto 1)) + ("0" & (vr_bv3 and vr_ua));
	vr_p4 <= ("0" & vr_p3(width downto 1)) + ("0" & (vr_bv4 and vr_ua));
	vr_p5 <= ("0" & vr_p4(width downto 1)) + ("0" & (vr_bv5 and vr_ua));
	vr_p6 <= ("0" & vr_p5(width downto 1)) + ("0" & (vr_bv6 and vr_ua));
	vr_p7 <= ("0" & vr_p6(width downto 1)) + ("0" & (vr_bv7 and vr_ua));
	
	vr_product <= vr_p7 & vr_p6(0) & vr_p5(0) & vr_p4(0) & vr_p3(0) & vr_p2(0) & 
	vr_p1(0) & vr_p0(0);
	
	VR_c <= std_logic_vector(vr_product);

end Behavioral;

