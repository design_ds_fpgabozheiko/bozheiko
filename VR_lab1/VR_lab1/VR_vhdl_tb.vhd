--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:13:49 11/30/2017
-- Design Name:   
-- Module Name:   D:/XI_LABS/VR_lab1/VR_vhdl_tb.vhd
-- Project Name:  VR_lab1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: VR_vhdl
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY VR_vhdl_tb IS
END VR_vhdl_tb;
 
ARCHITECTURE behavior OF VR_vhdl_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT VR_vhdl
    PORT(
         VR_a : IN  std_logic;
         VR_b : IN  std_logic;
         VR_c : IN  std_logic;
         VR_d : IN  std_logic;
         VR_e : IN  std_logic;
         VR_f : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal VR_a : std_logic := '0';
   signal VR_b : std_logic := '0';
   signal VR_c : std_logic := '0';
   signal VR_d : std_logic := '0';
   signal VR_e : std_logic := '0';

 	--Outputs
   signal VR_f : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: VR_vhdl PORT MAP (
          VR_a => VR_a,
          VR_b => VR_b,
          VR_c => VR_c,
          VR_d => VR_d,
          VR_e => VR_e,
          VR_f => VR_f
        );

   -- Clock process definitions
   VR_a <= not VR_a after 160 ns;
	VR_b <= not VR_b after 80 ns;
	VR_c <= not VR_c after 40 ns;
	VR_d <= not VR_d after 20 ns;
	VR_e <= not VR_e after 10 ns;

END;
