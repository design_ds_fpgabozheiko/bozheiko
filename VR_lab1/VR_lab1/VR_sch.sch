<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="VR_c" />
        <signal name="VR_a" />
        <signal name="VR_d" />
        <signal name="VR_e" />
        <signal name="VR_b" />
        <signal name="VR_f" />
        <port polarity="Input" name="VR_c" />
        <port polarity="Input" name="VR_a" />
        <port polarity="Input" name="VR_d" />
        <port polarity="Input" name="VR_e" />
        <port polarity="Input" name="VR_b" />
        <port polarity="Output" name="VR_f" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="VR_c" name="I0" />
            <blockpin signalname="XLXN_2" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="XLXN_4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="VR_f" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_3">
            <blockpin signalname="XLXN_1" name="I0" />
            <blockpin signalname="VR_a" name="I1" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_4">
            <blockpin signalname="VR_e" name="I0" />
            <blockpin signalname="VR_d" name="I1" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="VR_b" name="I" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="992" y="416" name="XLXI_1" orien="R0" />
        <instance x="1360" y="592" name="XLXI_2" orien="R0" />
        <instance x="640" y="384" name="XLXI_3" orien="R0" />
        <instance x="640" y="736" name="XLXI_4" orien="R0" />
        <instance x="352" y="352" name="XLXI_5" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="592" y1="320" y2="320" x1="576" />
            <wire x2="640" y1="320" y2="320" x1="592" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="992" y1="288" y2="288" x1="896" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1296" y1="320" y2="320" x1="1248" />
            <wire x2="1296" y1="320" y2="464" x1="1296" />
            <wire x2="1360" y1="464" y2="464" x1="1296" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1120" y1="640" y2="640" x1="896" />
            <wire x2="1120" y1="528" y2="640" x1="1120" />
            <wire x2="1360" y1="528" y2="528" x1="1120" />
        </branch>
        <branch name="VR_c">
            <wire x2="352" y1="432" y2="432" x1="288" />
            <wire x2="672" y1="432" y2="432" x1="352" />
            <wire x2="672" y1="352" y2="432" x1="672" />
            <wire x2="992" y1="352" y2="352" x1="672" />
        </branch>
        <branch name="VR_a">
            <wire x2="336" y1="256" y2="256" x1="288" />
            <wire x2="640" y1="256" y2="256" x1="336" />
        </branch>
        <branch name="VR_d">
            <wire x2="336" y1="608" y2="608" x1="288" />
            <wire x2="640" y1="608" y2="608" x1="336" />
        </branch>
        <branch name="VR_e">
            <wire x2="336" y1="672" y2="672" x1="288" />
            <wire x2="640" y1="672" y2="672" x1="336" />
        </branch>
        <branch name="VR_b">
            <wire x2="352" y1="320" y2="320" x1="288" />
        </branch>
        <branch name="VR_f">
            <wire x2="1744" y1="496" y2="496" x1="1616" />
        </branch>
        <iomarker fontsize="28" x="288" y="256" name="VR_a" orien="R180" />
        <iomarker fontsize="28" x="288" y="320" name="VR_b" orien="R180" />
        <iomarker fontsize="28" x="288" y="432" name="VR_c" orien="R180" />
        <iomarker fontsize="28" x="288" y="608" name="VR_d" orien="R180" />
        <iomarker fontsize="28" x="288" y="672" name="VR_e" orien="R180" />
        <iomarker fontsize="28" x="1744" y="496" name="VR_f" orien="R0" />
    </sheet>
</drawing>