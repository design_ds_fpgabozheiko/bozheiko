----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:12:53 12/13/2017 
-- Design Name: 
-- Module Name:    KP_K_mM_105 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KP_K_mM_105 is
    Port ( KP_A : in  STD_LOGIC_VECTOR (7 downto 0);
           KP_C_mM : out  STD_LOGIC_VECTOR (14 downto 0));
end KP_K_mM_105;
--constatnt K = 0110 1001
--optimized K = 0110 1001

architecture Behavioral of KP_K_mM_105 is
	signal KP_prod, KP_p0, KP_p3, KP_p5, KP_p6:unsigned(14 downto 0);
begin
	KP_p0  <= "0000000" & unsigned(KP_A);
	KP_p3  <= "0000" & unsigned(KP_A) & "000";
	KP_p5  <= "00" & unsigned(KP_A) & "00000";
	KP_p6  <= "0" & unsigned(KP_A) & "000000";
	KP_prod <= (KP_p6 + KP_p5) + (KP_p3 + KP_p0);
	KP_C_mM <= std_logic_vector(KP_prod);
end Behavioral;

