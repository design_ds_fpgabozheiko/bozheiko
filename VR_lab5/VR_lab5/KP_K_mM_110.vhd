----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:14:35 12/08/2017 
-- Design Name: 
-- Module Name:    KP_mMult_K - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KP_K_mM_110 is
    Port ( KP_A : in  STD_LOGIC_VECTOR (7 downto 0);
           KP_C_mM : out  STD_LOGIC_VECTOR (14 downto 0));
end KP_K_mM_110;
--constatnt K = 0110 1110
--optimized K = 1001 0010
--                 ^   ^                       
architecture Behavioral of KP_K_mM_110 is
	signal KP_prod, KP_p1, KP_p4, KP_p7:unsigned(14 downto 0);
begin
	KP_p1  <= "000000" & unsigned(KP_A) & "0";
	KP_p4  <= "000" & unsigned(KP_A) & "0000";
	KP_p7  <= unsigned(KP_A) & "0000000";
	KP_prod <= KP_p7 - KP_p4 - KP_p1 ;
	KP_C_mM <= std_logic_vector(KP_prod);
end Behavioral;

