-- Vhdl test bench created from schematic D:\XI_LABS\KP_lab5\KP_fir_fin_sh.sch - Wed Dec 13 16:58:46 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY KP_fir_fin_sh_KP_fir_fin_sh_sch_tb IS
END KP_fir_fin_sh_KP_fir_fin_sh_sch_tb;
ARCHITECTURE behavioral OF KP_fir_fin_sh_KP_fir_fin_sh_sch_tb IS 

   COMPONENT KP_fir_fin_sh
   PORT( KP_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          KP_Y	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_Y_ip	:	OUT	STD_LOGIC_VECTOR (16 DOWNTO 0); 
          KP_CE	:	IN	STD_LOGIC; 
          KP_CLR	:	IN	STD_LOGIC; 
          KP_CLK	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL KP_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"01";
   SIGNAL KP_Y	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL KP_Y_ip	:	STD_LOGIC_VECTOR (16 DOWNTO 0);
   SIGNAL KP_CE	:	STD_LOGIC:='1';
   SIGNAL KP_CLR	:	STD_LOGIC:='0';
   SIGNAL KP_CLK	:	STD_LOGIC:='0';

BEGIN

   UUT: KP_fir_fin_sh PORT MAP(
		KP_A => KP_A, 
		KP_Y => KP_Y, 
		KP_Y_ip => KP_Y_ip, 
		KP_CE => KP_CE, 
		KP_CLR => KP_CLR, 
		KP_CLK => KP_CLK
   );

	clk_process : process
	begin
		KP_CLK <= '0';
		wait for 150 us;
		KP_CLK <= '1';
		wait for 150 us;
	end process;
--	tb: process
--		begin
--			KP_CLR <= '0' after 4 ns;
--			KP_CE <= '1' after 3 ns;
--			wait for 15 ns;
--			KP_A <= "00000001";
--			wait for 16 ns;
--			KP_A <= "00000000";
--			wait;
--		end process;
	data_change_process : process(kp_clk)
	variable I : integer;
	begin
		for i in 0 to 3 loop
			if()
			KP_A <= conv_std_logic_vector(conv_integer(KP_A) + 1,8);
			wait for 20 ns;
		end loop;
		for i in 0 to 5 loop
			KP_A <= "00000000";
			wait for 20 ns;
		end loop;
		for i in 0 to 3 loop
			wait for 20 ns;
			KP_A <= conv_std_logic_vector(conv_integer(KP_A) + 2,8);
			wait for 20 ns;
		end loop;
		for i in 0 to 5 loop
			KP_A <= "00000000";
			wait for 20 ns;
		end loop;
	end process;
END;
