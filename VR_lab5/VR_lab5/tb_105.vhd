--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:20:27 12/13/2017
-- Design Name:   
-- Module Name:   D:/XI_LABS/KP_lab5/tb_105.vhd
-- Project Name:  KP_lab5
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: KP_K_mM_105
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_105 IS
END tb_105;
 
ARCHITECTURE behavior OF tb_105 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT KP_K_mM_105
    PORT(
         KP_A : IN  std_logic_vector(7 downto 0);
         KP_C_mM : OUT  std_logic_vector(14 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal KP_A : std_logic_vector(7 downto 0) := x"12";

 	--Outputs
   signal KP_C_mM : std_logic_vector(14 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: KP_K_mM_105 PORT MAP (
          KP_A => KP_A,
          KP_C_mM => KP_C_mM
        );

   -- Clock process definitions
   
 

   -- Stimulus process
   stim_proc: process
   begin		
      KP_A <= x"34" after 10 ns;
		wait for 10 ns;
   end process;

END;
