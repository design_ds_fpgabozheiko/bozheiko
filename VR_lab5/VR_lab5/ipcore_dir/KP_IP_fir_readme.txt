The following files were generated for 'KP_IP_fir' in directory
D:\XI_LABS\KP_lab5\ipcore_dir\

Opens the IP Customization GUI:
   Allows the user to customize or recustomize the IP instance.

   * KP_IP_fir.mif

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * KP_IP_fir.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * KP_IP_fir.ngc
   * KP_IP_fir.vhd
   * KP_IP_fir.vho
   * KP_IP_firCOEFF_auto0_0.mif
   * KP_IP_firCOEFF_auto0_1.mif
   * KP_IP_firCOEFF_auto0_2.mif
   * KP_IP_firCOEFF_auto0_3.mif
   * KP_IP_firfilt_decode_rom.mif

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * KP_IP_fir.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * KP_IP_fir.asy
   * KP_IP_fir.mif

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * KP_IP_fir.sym

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * KP_IP_fir_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * KP_IP_fir.gise
   * KP_IP_fir.xise
   * _xmsgs/pn_parser.xmsgs

Deliver Readme:
   Readme file for the IP.

   * KP_IP_fir_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * KP_IP_fir_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

