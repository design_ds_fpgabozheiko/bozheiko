--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:34:32 12/14/2017
-- Design Name:   
-- Module Name:   D:/XI_LABS/KP_lab5/test_ip.vhd
-- Project Name:  KP_lab5
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: KP_IP_fir
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
 
ENTITY test_ip IS
END test_ip;
 
ARCHITECTURE behavior OF test_ip IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT KP_IP_fir
    PORT(
         clk : IN  std_logic;
         rfd : OUT  std_logic;
         rdy : OUT  std_logic;
         din : IN  std_logic_vector(7 downto 0);
         dout : OUT  std_logic_vector(16 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal din : std_logic_vector(7 downto 0) := x"02";

 	--Outputs
   signal rfd : std_logic;
   signal rdy : std_logic;
   signal dout : std_logic_vector(16 downto 0);
   
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: KP_IP_fir PORT MAP (
          clk => clk,
          rfd => rfd,
          rdy => rdy,
          din => din,
          dout => dout
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for 10 ns;
		clk <= '1';
		wait for 10 ns;
   end process;
	data_change: process
	variable i: integer;
   begin		
		for i in 0 to 3 loop
			wait for 10 ns;
			din <= conv_std_logic_vector(conv_integer(din) + 1,8);
			wait for 10 ns;
		end loop;
		for i in 0 to 5 loop
			din <= "00000000";
			wait for 20 ns;
		end loop;
			for i in 0 to 3 loop
				wait for 10 ns;
				din <= conv_std_logic_vector(conv_integer(din) + 2,8);
				wait for 10 ns;
			end loop;
			for i in 0 to 5 loop
				din <= "00000000";
				wait for 20 ns;
			end loop;
		end process;

END;
